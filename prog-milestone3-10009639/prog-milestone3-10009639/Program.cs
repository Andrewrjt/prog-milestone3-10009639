﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace prog_milestone3_10009639
{
    public class pizzaNdrinks
    {
        public static Dictionary<string, double> drinks = new Dictionary<string, double>()
                                                                                  {
                                                                                     {"Lemonade", 1.70},
                                                                                     {"Coke", 2.80},
                                                                                     {"Juice", 1.90}
                                                                                   };
        public static Dictionary<string, Tuple<double, double>> pizza = new Dictionary<string, Tuple<double, double>>()
                                                                                                              {
                                                                                                              {"Hawaiian", Tuple.Create (1.40, 6.40) },
                                                                                                              {"Meatlovers", Tuple.Create (1.70,5.30) },
                                                                                                              {"Supreme", Tuple.Create(2.40, 5.70) }
                                                                                                              };
    }
    public class userInfo
    {
        public static string name = "Unknown";
        public static string address = "Unknown";
        public static string email = "Unknown";
        public static string phone = "Unknown";
    }
    public class order
    {
        public static List<Tuple<string, double>> pizzaNdrinkOrder = new List<Tuple<string, double>>();
    }
    class Program
    {
        static void Main(string[] args)
        {
            var repeat = true;
            while (repeat == true)
            {
                int menuchoice = 0;
                while (menuchoice != 5)
                {
                    Console.Clear();
                    Console.WriteLine("MENU");
                    Console.WriteLine("Please enter the number that you want to do:");
                    Console.WriteLine("1. Customer details");
                    Console.WriteLine("2. Pizza order");
                    Console.WriteLine("3. Drink order");
                    Console.WriteLine("4. Checkout");
                    Console.WriteLine("5. Exit");
                    var menu = Console.ReadLine();
                    var a = 0;
                    bool compare = int.TryParse(menu, out a);
                    if (compare == true)
                    {
                        var choice = Convert.ToInt32(menu);
                        menuchoice = choice;

                        switch (menuchoice)
                        {
                            case 1:
                                Console.Clear();
                                askForinfo();
                                break;
                            case 2:
                                Console.Clear();
                                askForPizza();
                                break;
                            case 3:
                                Console.Clear();
                                askForDrinks();
                                break;
                            case 4:
                                Console.Clear();
                                checkDetails();
                                getMoney();
                                repeat = false;
                                break;
                            case 5:
                                Console.Clear();
                                repeat = false;
                                menuchoice = 5;
                                break;
                            default:
                                Console.WriteLine("Sorry, invalid selection");
                                break;
                        }
                    }
                    else
                    {
                        Console.Clear();
                        Console.WriteLine("it needs to be a number from 1 to 5");
                        repeat = true;
                        break;
                    }
                }
            }
        }
        static void askForinfo()
        {
            Console.Clear();
            Console.WriteLine("Hello Can I please get the customers name");
            getName();
            Console.Clear();
            Console.WriteLine("now can i get the address");
            getAddress();
            Console.Clear();
            Console.WriteLine("Now i need the email");
            getEmail();
            Console.Clear();
            Console.WriteLine("now for the phone number");
            getPhone();
        }
        static void getName()
        {
            userInfo.name = Console.ReadLine();
        }
        static void getAddress()
        {
            userInfo.address = Console.ReadLine();
        }
        static void getEmail()
        {
            userInfo.email = Console.ReadLine();
        }
        static void getPhone()
        {
            userInfo.phone = Console.ReadLine();
        }
        static void askForPizza()
        {
            var repeat = true;
            while (repeat == true)
            {
                Console.Clear();
                Console.WriteLine($"1.Hawaiian Small:$1.40 Large:$6.40");
                Console.WriteLine($"2.Meatlovers Small:$1.70 Large:$5.30");
                Console.WriteLine($"3.Supreme Small:$2.40 Large:$5.70");
                Console.WriteLine("what pizza would you like to add? type in the number on the left to add otherwise press 4 to exit");
                var selection = "";
                var size = 0;
                var menu = Console.ReadLine();
                var a = 0;
                bool compare = int.TryParse(menu, out a);
                if (compare == true)
                {
                    int menu1 = Convert.ToInt32(compare);
                    switch (menu1)
                    {
                        case 1:
                            selection = "Hawaiian";
                            foreach (var x in pizzaNdrinks.pizza)
                            {
                                if (x.Key == selection)
                                {
                                    Console.WriteLine("Ok Hawaiian what size, 1 for small 2 for large");
                                    int option1 = int.Parse(Console.ReadLine());
                                    size = option1;
                                    switch (size)
                                    {
                                        case 1:
                                            order.pizzaNdrinkOrder.Add(Tuple.Create(x.Key, x.Value.Item1));
                                            break;
                                        case 2:
                                            order.pizzaNdrinkOrder.Add(Tuple.Create(x.Key, x.Value.Item2));
                                            break;
                                        default:
                                            Console.WriteLine("it needs to be 1 or 2");
                                            break;
                                    }
                                }
                            }
                            break;
                        case 2:
                            selection = "Meatlovers";
                            foreach (var x in pizzaNdrinks.pizza)
                            {
                                if (x.Key == selection)
                                {
                                    Console.WriteLine("Ok Meatlovers what size, 1 for small 2 for large");
                                    int option1 = int.Parse(Console.ReadLine());
                                    size = option1;
                                    switch (size)
                                    {
                                        case 1:
                                            order.pizzaNdrinkOrder.Add(Tuple.Create(x.Key, x.Value.Item1));
                                            break;
                                        case 2:
                                            order.pizzaNdrinkOrder.Add(Tuple.Create(x.Key, x.Value.Item2));
                                            break;
                                        default:
                                            Console.WriteLine("it needs to be 1 or 2");
                                            break;
                                    }
                                }
                            }
                            break;
                        case 3:
                            selection = "Supreme";
                            foreach (var x in pizzaNdrinks.pizza)
                            {
                                if (x.Key == selection)
                                {
                                    Console.WriteLine("Ok Supreme what size, 1 for small 2 for large");
                                    int option1 = int.Parse(Console.ReadLine());
                                    size = option1;
                                    switch (size)
                                    {
                                        case 1:
                                            order.pizzaNdrinkOrder.Add(Tuple.Create(x.Key, x.Value.Item1));
                                            break;
                                        case 2:
                                            order.pizzaNdrinkOrder.Add(Tuple.Create(x.Key, x.Value.Item2));
                                            break;
                                        default:
                                            Console.WriteLine("it needs to be 1 or 2");
                                            break;
                                    }
                                }
                            }
                            break;
                        case 4:
                            repeat = false;
                            break;
                    }
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("it needs to be a number press any key to continue");
                    Console.ReadLine();
                    repeat = true;
                    break;
                }
            }
        }
        static void askForDrinks()
        {
            var repeat = true;
            while (repeat == true)
            {
                Console.Clear();
                Console.WriteLine("1.Lemonade : $1.70");
                Console.WriteLine("2.Coke : $2.80");
                Console.WriteLine("3.Juice : $1.90");
                Console.WriteLine("what drinks would you like to add? type in the number on the left to add otherise press 4 to exit");
                var selection = "";
                var menu = Console.ReadLine();
                var a = 0;
                bool compare = int.TryParse(menu, out a);
                if (compare == true)
                {
                    int menu1 = Convert.ToInt32(compare);
                    switch (menu1)
                    {

                        case 1:
                            selection = "Lemonade";
                            foreach (var x in pizzaNdrinks.drinks)
                            {
                                if (x.Key == selection)
                                {
                                    order.pizzaNdrinkOrder.Add(Tuple.Create(x.Key, x.Value));
                                }
                            }
                            break;
                        case 2:
                            selection = "Coke";
                            foreach (var x in pizzaNdrinks.drinks)
                            {
                                if (x.Key == selection)
                                {
                                    order.pizzaNdrinkOrder.Add(Tuple.Create(x.Key, x.Value));
                                }
                            }
                            break;
                        case 3:
                            selection = "Juice";
                            foreach (var x in pizzaNdrinks.drinks)
                            {
                                if (x.Key == selection)
                                {
                                    order.pizzaNdrinkOrder.Add(Tuple.Create(x.Key, x.Value));
                                }
                            }
                            break;
                        case 4:
                            repeat = false;
                            break;
                    }
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("it needs to be a number press any key to continue");
                    Console.ReadLine();
                    repeat = true;
                    break;
                }
            }
        }
        static void checkDetails()
        {
            Console.WriteLine(userInfo.name);
            Console.WriteLine(userInfo.address);
            Console.WriteLine(userInfo.email);
            Console.WriteLine(userInfo.phone);
            Console.WriteLine("Are these details correct? type no to change or yes to continue.");
            var check = Console.ReadLine();
            switch (check)
            {

                case "no":
                    changeDetails();
                    break;
                case "yes":
                    break;
                default:
                    Console.WriteLine("it needs to be no or yes");
                    break;
            }
        }
        static void changeDetails()
        {
            var repeat = true;
            while (repeat == true)
            {
                Console.Clear();
                Console.WriteLine(userInfo.name);
                Console.WriteLine(userInfo.address);
                Console.WriteLine(userInfo.email);
                Console.WriteLine(userInfo.phone);
                Console.WriteLine("which details need to be corrected? type the number next to it to change or press 5 to continue");
                var check = Console.ReadLine();
                switch (check)
                {
                    case "1":
                        Console.WriteLine("Ok what is the new name");
                        getName();
                        break;
                    case "2":
                        Console.WriteLine("Ok what is the new address");
                        getAddress();
                        break;
                    case "3":
                        Console.WriteLine("Ok what is the new email");
                        getEmail();
                        break;
                    case "4":
                        Console.WriteLine("Ok what is the new phone");
                        getPhone();
                        break;
                    case "5":
                        repeat = false;
                        break;
                    default:
                        Console.WriteLine("it needs to be a number from 1 to 4 otherwise 5 to continue");
                        break;
                }
            }
        }
        static void getMoney()
        {
            var repeat = true;
            while (repeat == true)
            {
                Console.Clear();
                order.pizzaNdrinkOrder.ForEach(Console.WriteLine);
                var count = order.pizzaNdrinkOrder.Count;
                var i = 0;
                double sum = 0;
                for (i = 0; i < count; i++)
                {
                    sum += order.pizzaNdrinkOrder[i].Item2;
                }
                Console.Write($"the total is ${sum} how much is the customer paying?");
                var money = Console.ReadLine();
                var a = 0.00;
                bool compare = double.TryParse(money, out a);
                if (compare == true)
                {
                    double money1 = Convert.ToDouble(money);
                    Console.WriteLine($"OK that is ${sum - money1} in change");
                    Console.WriteLine("thank you for using this program");
                    Console.ReadLine();
                    repeat = false;
                }
                else
                {
                    Console.Clear();
                    Console.WriteLine("it needs to be a number press any key to continue");
                    Console.ReadLine();
                    repeat = true;
                }
            }
        }
    }
}
